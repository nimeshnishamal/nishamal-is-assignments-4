#include <stdio.h>
#include <stdlib.h>

int main()
{   float rad,height;
    printf("Enter the radius:\n");
    scanf("%f",&rad);
    printf("Enter the height: \n");
    scanf("%f",&height);
    double vol=3.14*rad*rad*(height/3);
    printf("volume of cone is : %.2lf",vol);
    return 0;
}
