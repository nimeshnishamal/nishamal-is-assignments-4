#include <stdio.h>
#include <stdlib.h>

int main()
{   int num1,num2,num3;
    printf("Enter number 1: \n");
    scanf("%d",&num1);
    printf("Enter number 2: \n");
    scanf("%d",&num2);
    printf("Enter number 3: \n");
    scanf("%d",&num3);
    int sum=num1+num2+num3;
    printf("Sum of these 3 numbers : %d \nAverage of these 3 numbers : %.3f ",sum,(float)sum/3);
    return 0;
}
