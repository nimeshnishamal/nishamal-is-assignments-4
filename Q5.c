#include <stdio.h>
#include <stdlib.h>

int main()
{   int A=10,B=15;
    printf("A : %d , B : %d\n\n",A,B);
    printf("1. A&B : %d & %d = %d\n",A,B,A&B);
    printf("2. A^B : %d ^ %d = %d\n",A,B,A^B);
    printf("3. ~A : %d\n",~A);
    printf("4. A<< : %d\n",A<<3);
    printf("5. B>>3 : %d\n",B>>3);
    return 0;
}
